from django.shortcuts import render

class SiteUnderConstructionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        print('This is before site under construction view')
        response = render(request, 'core/site_under_construction.html')
        print('This is after site under construction view')

        return response